import sys
import requests
from tabulate import tabulate
from grab import Grab
import sched, time

TIMEOUT = 10
s = sched.scheduler(time.time, time.sleep)


def do_request(search='Инфо'):
    g = Grab()
    g.go(
        'https://www.parter.ru/tickets.htm?affiliate=H16&doc=erdetaila&fun=erdetail&erid=1511768&sort_by=event_datum&sort_direction=asc&show=10&index=0&sort_by=event_datum')

    xpath = '//*[@id="detailaTable"]/tbody/tr[@class="eventRow"]'

    resp = []
    for element in g.doc.select(xpath):
        tickets = []
        who = element.select('*[@class="eventCol"]').text()
        who = who[who.index('/') + 2:]
        when = element.select('*[@class="dateCol current"]').text()
        info = element.select('*[@class="contentRight btnNext ticketsCol"]').text()

        # if 'РОССИЯ' in who and 'Билеты' in info:
        if 'РОССИЯ' in who and search in info:
            tickets.append(who)
            tickets.append(when)
            tickets.append(info)
            resp.append(tickets)

    if len(resp) > 0:
        return tabulate(resp)
    else:
        return 'Ничего не удалось найти'


def run_monitoring(sc):
    print(do_request())
    sc.enter(TIMEOUT, 1, run_monitoring, (sc,))


def start_monitoring():
    s.enter(0, 1, run_monitoring, (s,))
    s.run()


def stop_monitoring():
    s.cancel()


def main(argv):
    print(argv)
    if len(argv) > 1:
        if argv[1] == 'start':
            start_monitoring()
        elif argv[1] == 'stop':
            stop_monitoring()
    else:
        print(do_request())


# if __name__ == "__main__":
#     main(sys.argv)
    # print(do_request())
