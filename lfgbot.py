from random import getrandbits
import re

from telegram import InlineQueryResultArticle, ParseMode
from telegram.ext import Updater
import logging

from superheroes import search
from superheroes import superpowers

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

logger = logging.getLogger(__name__)

TOKEN = '190318763:AAEcE2PQH0yZHYdc0oj8UTjktQU1zIy6C7c'

ARTEM = ['артем', 'артём']
LESHA = ['леша']
GIRO = ['гироскоп']


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    bot.sendMessage(update.message.chat_id, text='Hi!')


def help(bot, update):
    bot.sendMessage(update.message.chat_id, text='Help!')


def escape_markdown(text):
    """Helper function to escape telegram markup symbols"""
    escape_chars = '\*_`\['
    return re.sub(r'([%s])' % escape_chars, r'\\\1', text)


def parse_message(bot, update):
    if any(x in update.message.text.lower() for x in ARTEM):
        bot.sendMessage(update.message.chat_id, text='Гироскоп!')
    if any(x in update.message.text.lower() for x in LESHA):
        bot.sendMessage(update.message.chat_id, text='Шоксель')
    if any(x in update.message.text.lower() for x in GIRO):
        bot.sendPhoto(update.message.chat_id, photo=open('giro.jpg', 'rb'))


def inlinequery(bot, update):
    if update.inline_query is not None and update.inline_query.query:
        query = update.inline_query.query
        results = list()

        print('query=' + query)
        for hero in search(query):
            # print(hero)
            results.append(InlineQueryResultArticle(
                id=hex(getrandbits(64))[2:],
                title=hero,
                message_text=superpowers(hero)))

        bot.answerInlineQuery(update.inline_query.id, results=results)


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))


def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater(TOKEN)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.addTelegramCommandHandler("start", start)
    dp.addTelegramCommandHandler("help", help)

    # on noncommand i.e message - echo the message on Telegram
    # dp.addTelegramInlineHandler(inlinequery)

    # on noncommand i.e message contains Artem name
    dp.addTelegramMessageHandler(parse_message)

    # log all errors
    dp.addErrorHandler(error)

    # Start the Bot
    updater.start_polling()

    # Block until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
