import sys
import urllib.parse


def main(argv):
    print(argv)


if __name__ == "__main__":
    main(sys.argv)
    print(urllib.parse.urlencode({'': u'Бэтмен: Под красным колпаком'.replace(' ','_').encode('utf-8')}))
    hero='Бэтмен: Под красным колпаком'
    # https://ru.wikipedia.org/wiki/%D0%91%D1%8D%D1%82%D0%BC%D0%B5%D0%BD:_%D0%9F%D0%BE%D0%B4_%D0%BA%D1%80%D0%B0%D1%81%D0%BD%D1%8B%D0%BC_%D0%BA%D0%BE%D0%BB%D0%BF%D0%B0%D0%BA%D0%BE%D0%BC
    url='https://ru.wikipedia.org/wiki/'+urllib.parse.urlencode({'': hero.replace(' ','_').encode('utf-8')})[1:]
    print(url)
