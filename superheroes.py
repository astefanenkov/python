import sys
import wikipedia
import urllib.parse
import re
from grab import Grab


def main(argv):
    print(argv)

def escape_markdown(text):
    """Helper function to escape telegram markup symbols"""
    escape_chars = '\*_`\['
    return re.sub(r'([%s])' % escape_chars, r'\\\1', text)


def search(hero):
    wikipedia.set_lang("ru")
    return wikipedia.search(hero, results=5, suggestion=False)


def superpowers(hero):
    powers = []
    # page = wikipedia.page(hero)
    g = Grab()
    print('hero=' + hero)
    url = 'https://ru.wikipedia.org/wiki/' + urllib.parse.urlencode({'': hero.replace(' ', '_').encode('utf-8')})[1:]
    print('url=' + url)
    g.go(url)
    xpath = "//tr[* = 'Особые силы']/following-sibling::tr[1]/td/ul/li"
    for element in g.doc.select(xpath):
        powers.append("*"+element.text())

    if len(powers) > 0:
        reponse = []
        reponse.append("Суперспособности героя " + hero + ":")
        reponse.extend(powers)
        more="Подробнее: " + url
        reponse = "\n".join(reponse)[:400-len(more)-2]
        reponse = reponse+"\n"+more
    else:
        reponse = 'Не удалось найти. Это герой? ' + url
    print(reponse)
    return reponse


if __name__ == "__main__":
    main(sys.argv)
    wikipedia.set_lang("ru")
    print(wikipedia.suggest("Супермен"))
